package db

import (
	"fmt"

	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

type PostgresClient struct {
	DB *sqlx.DB
}

func NewPostgresClient(dsn string) (*PostgresClient, error) {
	client, err := sqlx.Connect("pgx", dsn)
	if err != nil {
		return nil, fmt.Errorf(
			"cannot connect to the postgresql database %v err : %v",
			dsn, err,
		)
	}
	if err := client.Ping(); err != nil {
		return nil, fmt.Errorf("error while ping to postgres")
	}
	// TODO Need to set this values from config
	//cxDB.SetMaxOpenConns()
	//cxDB.SetMaxIdleConns()
	//cxDB.SetConnMaxLifetime()
	return &PostgresClient{client}, nil
}
