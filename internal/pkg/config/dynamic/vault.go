package dynamic

import (
	"fmt"
	"net/http"
	"time"

	"github.com/hashicorp/vault/api"

	"vault-example/internal/pkg/config/static"
)

type VaultClient struct {
	client *api.Client
	paths  Paths
}

type Paths struct {
	Postgres string
}

func NewVaultClient(cfg static.Vault) (*VaultClient, error) {
	httpClient := &http.Client{Timeout: time.Second * 10}

	client, err := api.NewClient(&api.Config{Address: cfg.Addr, HttpClient: httpClient})
	if err != nil {
		return nil, err
	}
	client.SetToken(cfg.Token)
	vaultClient := &VaultClient{client: client}
	vaultClient.mapConfigPaths(cfg.Path)
	return vaultClient, nil
}

func (v *VaultClient) mapConfigPaths(path static.VaultPaths) {
	v.paths.Postgres = path.Postgres
}

func (v *VaultClient) Read(path string) (*api.Secret, error) {
	return v.client.Logical().Read(path)
}

func (v *VaultClient) PostgresConfig() (Postgres, error) {
	path := v.paths.Postgres
	secret, err := v.client.Logical().Read(path)
	if err != nil {
		return Postgres{}, err
	}
	if secret == nil {
		return Postgres{}, fmt.Errorf("secret is nil under path: %v", path)
	}
	// if you use v1 engine for key/value all key values will be store right into secret.Data map
	dsn, ok := secret.Data["dsn"].(string)
	if !ok {
		return Postgres{}, fmt.Errorf("dsn key is not a string")
	}
	return Postgres{DSN: dsn}, nil
}
