package static

import (
	"fmt"
	"strings"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

type Config struct {
	Vault Vault `mapstructure:"vault"`
}

func NewConfig() (*Config, error) {
	cfg := &Config{}
	v := viper.New()

	v.SetDefault("vault.addr", "example:6831")
	v.SetDefault("vault.token", "root")
	v.SetDefault("vault.paths.postgres", "app/v1/example/postgres")

	v.AutomaticEnv()
	v.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))
	if err := v.Unmarshal(cfg, viper.DecodeHook(
		mapstructure.ComposeDecodeHookFunc(
			mapstructure.StringToTimeDurationHookFunc(),
			mapstructure.StringToSliceHookFunc(","),
		))); err != nil {
		return nil, fmt.Errorf("cannot read unmarshal: %w", err)
	}
	return cfg, nil
}
