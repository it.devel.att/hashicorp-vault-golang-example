package static

type Vault struct {
	Addr  string     `mapstructure:"addr"`
	Token string     `mapstructure:"token"`
	Path  VaultPaths `mapstructure:"paths"`
}

type VaultPaths struct {
	Postgres string `mapstructure:"postgres"`
}
