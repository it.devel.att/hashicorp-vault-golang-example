#### Prepare Vault
Run Vault

```shell script
docker-compose up
```
You will output like this
```shell script
dev-vault | WARNING! dev mode is enabled! In this mode, Vault runs entirely in-memory
dev-vault | and starts unsealed with a single unseal key. The root token is already
dev-vault | authenticated to the CLI, so you can immediately begin using Vault.
dev-vault | 
dev-vault | You may need to set the following environment variable:
dev-vault | 
dev-vault |     $ export VAULT_ADDR='http://0.0.0.0:8200'
dev-vault | 
dev-vault | The unseal key and root token are displayed below in case you want to
dev-vault | seal/unseal the Vault or re-authenticate.
dev-vault | 
dev-vault | Unseal Key: wETndQNg0KhFBKHvrHpsNZGbdmzSGa7Wg0VEA7/yJ6I=
dev-vault | Root Token: 123
dev-vault | 
dev-vault | Development mode should NOT be used in production installations!
```
Our interest is **Root Token**

Go inside vault container
```shell script
docker exec -it dev-vault sh
```
Inside container export token from console
```shell script
export VAULT_TOKEN=123
```
> It's **123** because we set **VAULT_DEV_ROOT_TOKEN_ID: 123** in docker-compose for vault

Enable KV engine for path `app/tracking/production/v1/secrets/`
> By default, this command create v1 engine
```shell script
vault secrets enable -path="app/tracking/production/v1/secrets/" kv
```
> For example our app **tracking** and we want to get **production** secrets under **v1** path

Put dsn for database connection secret in **app/tracking/production/v1/secrets/postgres** path
```shell script
vault kv put app/tracking/production/v1/secrets/postgres dsn=postgresql://john:strongpassword123@127.0.0.1:5433/tracking-app
```
#### Run application
Export .env values
```shell script
export $(cat .env)
```
Run main.go file
```shell script
go run cmd/app/main.go
```
If all ok you must see
```shell script
Success ping postgres
```
Which means we successfully read config from vault and get dsn for connect to postgres


UI of Vault available on [localhost:8200](http://localhost:8200/ui)