package main

import (
	"fmt"
	"vault-example/internal/pkg/db"

	"vault-example/internal/pkg/config/dynamic"
	"vault-example/internal/pkg/config/static"
)

func main() {
	cfg, err := static.NewConfig()
	if err != nil {
		panic(err)
	}
	client, err := dynamic.NewVaultClient(cfg.Vault)
	if err != nil {
		panic(err)
	}
	psqlCfg, err := client.PostgresConfig()
	if err != nil {
		panic(err)
	}
	psqlClient, err := db.NewPostgresClient(psqlCfg.DSN)
	if err != nil {
		panic(err)
	}
	if err := psqlClient.DB.Ping(); err != nil {
		panic(err)
	}
	fmt.Println("Success ping postgres")
}
